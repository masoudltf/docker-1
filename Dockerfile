FROM novinrepo:8082/docker/node:slim

COPY .npmrc /root/.npmrc

WORKDIR /app

COPY app/package*.json .
RUN npm install

COPY app .
CMD ["node","index.js"]