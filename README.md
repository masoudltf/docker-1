# Docker - Example #1
In this example we create a simple express.js server and configure docker on it.

## Run project 
```bash
# You just need execute below command to run project (in project root folder)
docker-compose up

after that, server runs in port 9000
```

## URLs
```bash
http://localhost:9000/hello => return "Hello Stranger"
http://localhost:9000/hello?name=SomeName => return "Hello SomeName"
http://localhost:9000/author => return "Masoud Lotfi"
```