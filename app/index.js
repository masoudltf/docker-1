const express = require('express')
const app = express()
const port = 3000;

const winston = require('winston');
const {format} = winston;
const {combine, timestamp, printf} = format;

const myFormat = printf(({level, message, timestamp}) => {
    return `${timestamp} [${level}]: ${message}`;
});
const logger = winston.createLogger({
    level: 'info',
    format: combine(
        timestamp(),
        myFormat
    ),
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({filename: 'error.log', level: 'error'}),
        new winston.transports.File({filename: 'combined.log'}),
    ]
});

app.get('/hello', (req, res) => {
    logger.info(`Request received:\n ${JSON.stringify(req.query)}`);
    let msg = 'Stranger';
    if (req.query.name) {
        msg = req.query.name;
    }
    res.send(`Hello ${msg}`);
});

app.get('/author', (req, res) => {
    logger.info(`Request received:\n ${JSON.stringify(req.query)}`);
    res.send('Masoud Lotfi');
});

app.listen(port, () => {
    logger.info(`Server running at port ${port}`);
})